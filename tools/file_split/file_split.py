#! /usr/bin/python
#
#	file_split.py
#
#						Apr/09/2022
#
import	sys
#
#
# --------------------------------------------------------------------
def file_split_proc(lines):
	sys.stderr.write("len(lines) = %d\n" % len(lines))
	count = 0
	nline = 0
	fp_out = open("tmp001.txt",mode='w',encoding='utf-8')
	for line in lines:
		if line[0] == '♪':
			fname=line[1:-1] + ".txt"
			fp_out.close()
			fp_out = open(fname,mode='w',encoding='utf-8')
			print(fname)
			count += 1
		if 5000 < count:
			break
#
		fp_out.write(line)
		nline += 1
#
	fp_out.close()
#
	sys.stderr.write("nline = %d\n" % nline)
# --------------------------------------------------------------------
sys.stderr.write("*** 開始 ***\n")
file_in = sys.argv[1]
#
try:
	fp_in = open(file_in,encoding='utf-8')
	lines = fp_in.readlines()	
	file_split_proc(lines)
except Exception as ee:
	sys.stderr.write("*** error *** in text_read_proc ***\n")
	sys.stderr.write(str(ee) + '\n')
#
sys.stderr.write("*** 終了 ***\n")
# --------------------------------------------------------------------
