#! /usr/bin/python3
#
#	maria_read.py
#					Apr/10/2022
#
import sys
#
import mysql.connector
#
#
# ----------------------------------------------------------------
sys.stderr.write("*** 開始 ***\n")
#
host_aa='localhost'
data_base = 'jei'
user_aa ='scott'
password_aa = 'tiger123'
conn = mysql.connector.connect(user=user_aa, password=password_aa, \
                              host=host_aa,database=data_base)
#
cursor = conn.cursor(dictionary=True)
#
#sql_str="select * from njev where  numero = '6017300'"
sql_str="select * from njev where  numero = '0016300'"
#sql_str=u"select * from njev where speco = 'midasi' and  numero = '0016300'"
cursor.execute (sql_str)
rows = cursor.fetchall ()
sys.stderr.write("len(rows) = %d\n" % len(rows))
for row in rows:
	print(row)
#
cursor.close()
conn.close()
#
#
sys.stderr.write("*** 終了 ***\n")
#
# ----------------------------------------------------------------
