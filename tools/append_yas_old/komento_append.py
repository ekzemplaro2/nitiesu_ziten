#! /usr/bin/python3
#
#	komento_append.py
#					Apr/11/2022
#
import sys
#
import mysql.connector
#
#
# ----------------------------------------------------------------
def get_max_idrik_proc(cursor,name_table):
	sql_str="select max(idrik) from " + name_table
	cursor.execute(sql_str)
	row = cursor.fetchone()
	max = row['max(idrik)']
	print(max)
#
	return max
# ----------------------------------------------------------------
def komento_append_proc(cursor,name_table,numero_in,dateno):
	max_idrik = get_max_idrik_proc(cursor,name_table)
	sql_str="select * from njev where speco = 'yakugo' and  numero = '" + numero_in + "'"
	cursor.execute (sql_str)
	rows = cursor.fetchall ()
	sys.stderr.write("len(rows) = %d\n" % len(rows))
	for row in rows:
		print(row)
	print()
#
	data_new = rows[0]
	data_new['idrik'] = max_idrik + 1
	data_new['speco'] = 'komento'
	data_new['dateno'] = dateno

	columns = ', '.join("`" + str(x).replace('/', '_') + "`" for x in data_new.keys())
	values = ', '.join("'" + str(x).replace('/', '_') + "'" for x in data_new.values())
	sql = "INSERT INTO %s ( %s ) VALUES ( %s );" % (name_table, columns, values)
	print(sql)

	cursor.execute(sql)
#
# ----------------------------------------------------------------
sys.stderr.write("*** 開始 ***\n")
#
host_aa='localhost'
data_base = 'jei'
user_aa ='scott'
password_aa = 'tiger123'
#
name_table = 'njev'
conn = mysql.connector.connect(user=user_aa, password=password_aa, \
                              host=host_aa,database=data_base)
#
cursor = conn.cursor(dictionary=True)
#
numero_in = '6017300'
dateno = 'Apr/11/2022 PM 16:01'
komento_append_proc(cursor,name_table,numero_in,dateno)

conn.commit()
cursor.close()
conn.close()
#
#
sys.stderr.write("*** 終了 ***\n")
#
# ----------------------------------------------------------------
